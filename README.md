# Smart Contracts for Certified and Sustainable Safety-Critical Continuous Monitoring Applications

This repository contains the source code for implementing a Hyperledger Fabric blockchain-based system which enables certified removal of sensors data in Continuous Monitoring application databases according to the methodology discussed in "Elia, N., Barchi, F., Parisi, E., Pompianu, L., & Acquaviva, A. (2022). Smart Contracts for Certified and Sustainable Safety-Critical Continuous Monitoring Applications.".

The framework allows for the deployment of data-evaluation policies to classify the chunks of data flowing into the database, assigning them an expiration date.

The novelty of this approach, with respect to standard data-reduction systems, stands in the implementation of the data-evaluation policy as a smart contract. This allows the system to be deployed on a blockchain in a either public or permissioned distributed fashion.
Since data deletion decisions came from the Policy Smart Contract, the underlying blockchain guarantees that critical removal database operations are tamper-proof and compliant with the guideline determined by system stakeholders.
Indeed, the smart contract may be deployed only if all the stakeholders endorse it.

Cite the author's work using:
```
@book{Elia2022Aug,
	author = {Elia, Nicola and Barchi, Francesco and Parisi, Emanuele and Pompianu, Livio and Acquaviva, Andrea},
	title = {{Smart Contracts for Certified and Sustainable Safety-Critical Continuous Monitoring Applications}},
	journal = {ResearchGate},
	year = {2022},
	month = aug,
	isbn = {978-3-031-15739-4},
	doi = {10.1007/978-3-031-15740-0_27}
}
```

## Description

The high-level architecture of the system is as follows:

![Architecture](images/conceptFlow.png)

Different from the standard data-reduction systems, the reduction of data is not performed by the cloud layer, but by a smart contract.

Business stakeholders will always be able to see the data-evaluation policy which is deployed on the blockchain, and to query the Policy Smart Contract to check that the expiration dates stored withn the database are not tampered with.

At the same time, scientific stakeholders will have the ability to access a fast database for performing time-series analysis.

### Policy Gateway

Service that retrieves data from the network, assembles data chunks with unique IDs, inserts them into the database and triggers the Policy Smart Contract to apply the policy and establish their expiry date.

### Policy Watchdog

Service that periodically triggers the blockchain to get the ID of expired data chunks; eventually deletes expired chunks from the database and notifies their deletion to the blockchain

### Policy smart contract

The proposed Policy Smart Contract is a smart contract that allows for the definition of the expiration date of data chunks. It may store different policies for different data sources.

The Smart Contract has been developed as a Go chaincode, which exploits the Hyperledger Fabric SDK for building a Smart Contract. It defines the asset to be stored by the blockchain, and a set of available transactions.

The *asset* is defined as a data structure which holds:

- `AppliedPolicyId`: unique policy string identifier, which allows to keep track of which policy have been applied to the asset. May be empty if any policy has been assigned to the asset yet;
- `ChunkId`: unique asset string identifier, received along with a data chunk from the Policy Gateway;
- `DataHash`: md5 hash of the data chunk time series, computed at data chunk arrival and stored as a string;
- `ExpiryDate`: the expiry date of the asset, computed considering the block creation date, which is a property assigned by the blockchain itself, and the expiry period. May be empty if any policy has been assigned to the asset yet;
- `ExpiryPeriod`: the expiry time lapse, expressed as a time interval, computed by the applied policy. May be empty if any policy has been assigned to the asset yet.

The asset is designed such that sensors data is never stored within the blockchain, avoiding it to grow without tangible benefits. Indeed, it will always be possible to recover the chunk data from the system database using the `ChunkId` unique identifier.

Our Smart Contract is intended to provide support for different policies, that may run on assets data based on clients requirements. Therefore, a policy is defined as a class which contains a unique string identifier or `PolicyId`, and a function which receives a chunk of data as input, executes the policy-specific logic and outputs an expiry period, `PolicyLogic(inputData)`. As a consequence, multiple policy instances may be deployed within the Smart Contract, each one implementing its own logic. This gives support to heterogeneous data processing, allowing to apply different policies based on different inputs in a **multi-policy** approach.

A set of atomic *private transactions* are defined to access the World State and the chain blocks. They implement CRUD operations over the World State database, which may be used by the transaction functions to read, create, delete or update the stored assets, and read operations over the chain blocks, which may be used for retrieving data from past transactions.
These transactions can be invoked by *public transactions*, which are on the other hand available to the Smart Contract users for requesting them. Following, we list the main public transactions along with their explanation.

Transactions which are available to the user are:

- `AddChunk(chunkId, chunkData)`: receives chunk ID and chunk data as parameters, deserializes the chunk data as JSON document, computes its md5 hash, and creates a corresponding asset. The asset is then committed to the World State, which will therefore be part of the blockchain as soon as its nodes endorse the transaction.
- `AddChunkWithPolicy(chunkId, chunkData, policyId)`: receives chunk ID, chunk data and the policy identifier as parameters, computes the data chunk md5 hash, triggers the given policy to establish an expiry time lapse and creates a corresponding asset. The asset is then committed to the World State, which will therefore be part of the blockchain as soon as its nodes endorse the transaction.
- `ApplyPolicy(chunkId, chunkData, policyId)`: applies the given policy to the given asset. It is mandatory to include chunk data as parameter, because the blockchain does not store sensors data.
- `UpdateChunkExpiryDate(chunkId)`: updates the given asset with a deterministic expiry time. This transaction reads the given asset from the World State, and if it exists, then the getAssetCreationTime() private transaction is invoked for getting the creation time of the asset. The asset gets therefore updated in the world state. This approach makes impossible to counterfeit the creation date of the asset, thus making impossible for clients to apply the policy in a fraudulent manner.
- `GetExpiredChunks(expiryDateRFC3339)`: returns all the assets found in world state which are expired to the given expiry date. If the expiry date belongs to the future, the transaction returns an error.
- `DeleteChunkIfExpired(chunkId)`: deletes the given asset from the World State if and only if it has expired. Therefore, if a policy has not been applied yet, the chunk cannot be deleted. This transaction is intended to provide a way to Policy Watchdog for notifying to the blockchain the deletion of a data chunk from the database. Moreover, this approach allows the World State to be purged from assets bound to deleted chunk, improving the query performances.
- `ChunkExisted(chunkId)`: returns true if the given chunk appears at least one time within the blockchain. it makes uses of the private getAssetHistory() transaction for querying the chain blocks for the existence of any transaction involving the given asset.

Private transactions and methods that allow the transactions to work with the World State are:

- `createAsset(asset)`, `readAsset(assetId)`, `updateAsset(asset)`, `deleteAsset(assetId)`,  `assetExists(assetId)`: implement CRUD operations against the World State.
- `getAssetHistory(assetId)`: returns the chain of transactions for the given asset since its creation, by accessing the data contained within the blockchain blocks.
- `updateAssetExpiryDate(assetId, expiryDate)`: updates the ExpiryDate field of the given asset with the given date.
- `computeExpiryDate(assetId, expiryPeriod)`: returns a deterministic expiry date, based on the asset creation time - obtained by invoking `getAssetCreationTime()` - and the given expiry period.
- `getAssetCreationTime(assetId)`: invokes `getAssetHistory()` for obtaining the given asset transaction history and returns its creation time based on the timestamp of the first transaction.

## How it works

![How it works](images/blockchain_flow_nb_3.svg)

Policy Gateway receives a data chunk from sensor network (1) through an MQTT broker.
Then, it inserts the data chunk into the database (2a) and, at the same time, requests a transaction (A, AddChunk-WithPolicy) to store chunk information into the blockchain (2b).
If previous requests are successful, *Policy Gateway* triggers the Policy Smart Contract to compute the chunk expiry date (**B** - `UpdateChunkExpiryDate`).

When the chunk has expired, its unique ID will be included in the list returned by (**C** - `GetExpired-Chunks`) transaction.

Periodically, *Policy Watchdog* triggers the transaction (**C** - `GetExpiredChunks`) to receive (**4**) the IDs of expired chunks from the blockchain.

Eventually, it issues delete requests against the database (**5**).

If data removal is successful, *Policy Watchdog* notifies it to the *blockchain* (**6**) by triggering the transaction (**D** - `DeleteChunkIfExpired`).

## Setup

To test the Smart Contract, we need to setup a Fabric network.

For a quick test, clone the [fabric-samples](https://github.com/hyperledger/fabric-samples/tree/bb57b3cf8f07e032373ca796eaf12c78e97d7bbe) repository and setup the Fabric network as explained in the `fabric-samples/test-network/README.md` docs.

Then, run the network creating at least 2 Organizations and a channel between them.

The Smart Contract is located in `./src/chaincode-go` and can be deployed on the channel. A setup example is provided in `./fabric_testnet_setup_example.sh`.

Policies are defined in `./src/chaincode-go/policies`. One may create as many policies as needed, based aon the needs of the use-case. Two example policies are provided.

`./src/policygateway` and `./src/policywatchdog` contain respectively the Policy Gateway and the Policy Watchdog implemented as Go applications.

## Usage

It is possible to use the Policy Gateway and the Policy Watchdog applications to exploit the Smart Contract within the context of a Continuous Monitoring application. The Policy Gateway application must be connected with the data source (MQTT broker in the sample implementation), and the Policy Watchdog must be connected with the database for making *delete* requests.
