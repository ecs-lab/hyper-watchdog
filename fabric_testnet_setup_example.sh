#!/bin/bash -x

ENABLE_LOGSPOUT=false

for i in "$@"; do
  case $i in
    --logspout)
      ENABLE_LOGSPOUT=true
      shift # past argument with no value
      ;;
    -*|--*)
      echo "Unknown option $i"
      exit 1
      ;;
    *)
      ;;
  esac
done

# Set the desired script output
export PS4='[${0##*/}:${LINENO}] >>> ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
# Set the environment variable GOPATH to the project path
export GOPATH="/fabric-samples"
clear

# Script begins
# ref.: https://hyperledger-fabric.readthedocs.io/en/latest/write_first_app.html
cd ../test-network || exit
echo "Moved to: ${PWD}"

# Network start
# -----------------------------------
# Teardown any previous test network
./network.sh down

# Deploy the Fabric test network with two peers, an ordering service, and three certificate 
# authorities (Orderer, Org1, Org2). Instead of using the cryptogen tool, we bring up the test
# network using certificate authorities, hence the -ca flag. Additionally, the org admin user
# registration is bootstrapped when the certificate authority is started.

# Launch containers and create the default channel 'mychannel' with two channel members, Org1 and Org2.
./network.sh up createChannel -c mychannel -ca

# Logspout start
# -----------------------------------
if [ $ENABLE_LOGSPOUT == true ]
then
    echo "Logspout has been requested. Launching..."
    gnome-terminal -x sh -c "./monitordocker.sh fabric_test; bash"
    # Wait for Logspout to be ready and running
    sleep 5s
fi

# Deploy of the smart contract
# -----------------------------------
# We can now use the Peer CLI to deploy the policy smartcontract chaincode to the channel.
./network.sh deployCC -ccn policy -ccp ./chaincode-go/ -ccl go -c mychannel

# The previous command uses the `chaincode lifecycle` to package, install, query installed chaincode, 
# approve chaincode for both Org1 and Org2, and finally commit the chaincode.

# Stop and cleanup
# -----------------------------------
echo "If you use Logspout, please manually kill it before proceeding."
read -p "Press ENTER to bring down the network."

# Destroy containers
./network.sh down
