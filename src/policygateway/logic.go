package main

import (
	"log"
	"time"

	fabricGateway "github.com/hyperledger/fabric-sdk-go/pkg/gateway"
)

func runLogic(contract fabricGateway.Contract, dataChan chan Data) {
	count := 0

	for {
		select {
		case d := <-dataChan:
			count += 1
			log.Println("runTest: processing data with ID:", d.Id, "count:", count)

			// Policy gateway
			func() {
				// Policy Gateway: AddChunkWithPolicy
				func() {
					defer timeTrackLog(time.Now(), "AddChunkWithPolicy")
					log.Println("--> Policy Gateway: AddChunkWithPolicy")

					enc, err := Base64EncodeJson(d)
					if err != nil {
						log.Printf("Failed to encode Json data block: %v\n", err)
					}

					policyId := "signal_energy_policy_v1"

					result, err := contract.SubmitTransaction("AddChunkWithPolicy", d.Id, enc, policyId)
					if err != nil {
						log.Printf("Failed to Submit transaction: %v\n", err)
					}
					if len(result) == 0 {
						log.Println("OK")
					} else {
						log.Println(string(result))
					}
				}()

				// Policy Gateway: UpdateChunkExpiryDate
				func() {
					defer timeTrackLog(time.Now(), "UpdateChunkExpiryDate")
					log.Println("--> Policy Gateway: UpdateChunkExpiryDate")

					result, err := contract.SubmitTransaction("UpdateChunkExpiryDate", d.Id)
					if err != nil {
						log.Printf("Failed to Submit transaction: %v\n", err)
					}
					if len(result) == 0 {
						log.Println("OK")
					} else {
						log.Println(string(result))
					}
				}()
			}()
		}
	}
}
