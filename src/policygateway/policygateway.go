package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"policygateway/config"
	"policygateway/mqtt"
	"sync"
	"time"
)

var c *config.Configuration = config.GetCurrentConfig()

// Function to receive data from MQTT broker
func dataReceiver(dataChan chan<- Data) {
	mqttDataChan := mqtt.SubscribeAndListen()

	for {
		select {
		case msg := <-mqttDataChan:
			data := Data{} // For Json unmarshaling
			json.Unmarshal([]byte(msg), &data)

			log.Println("dataReceiver: received data with ID:", data.Id)
			data.ReceivedOn = time.Now()

			dataChan <- data
		}
	}
}

func main() {
	var wg sync.WaitGroup

	// flags
	readOnly := flag.Bool("r", false, "makes a GetAllAssets transaction and exits")
	flag.Parse()

	// initialize fabric SDK
	contract := setupFabricSDK()

	if *readOnly {
		func() {
			log.Println("--> Evaluate Transaction: GetAllAssets, function returns expired chunks")
			result, err := contract.EvaluateTransaction("GetAllAssets", time.Now().Format(time.RFC3339))
			if err != nil {
				log.Printf("Failed to evaluate transaction: %v\n", err)
			}
			if len(result) == 0 {
				log.Println("Empty list")
			} else {
				log.Println(string(result))
			}
		}()

		os.Exit(0)
	}

	// create a channel to route data received from MQTT broker
	wg.Add(1)
	dataChan := make(chan Data)
	go dataReceiver(dataChan)

	// run test
	wg.Add(1)
	go runLogic(contract, dataChan)

	wg.Wait()
}

func timeTrackLog(start time.Time, name string) {
	elapsed := time.Since(start)
	log.Printf("timeTrackLog: %s took %s", name, elapsed)
}

func durationToMsString(d time.Duration) string {
	ms := d / time.Millisecond
	return fmt.Sprintf("%d", ms)
}
