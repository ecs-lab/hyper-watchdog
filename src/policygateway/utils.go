package main

import (
	"bytes"
	"crypto/md5"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"strings"
)

func Base64Encode(str string) string {
	return base64.StdEncoding.EncodeToString([]byte(str))
}

func Base64Decode(str string) (string, error) {
	data, err := base64.StdEncoding.DecodeString(str)
	if err != nil {
		return "", errors.New("unable to decode base64 string")
	}
	return string(data), nil
}

func Base64EncodeJson(v interface{}) (string, error) {
	var buf bytes.Buffer
	encoder := base64.NewEncoder(base64.StdEncoding, &buf)
	err := json.NewEncoder(encoder).Encode(v)
	if err != nil {
		return "", err
	}
	encoder.Close()
	return buf.String(), nil
}

func Base64DecodeJson(v interface{}, enc string) error {
	return json.NewDecoder(base64.NewDecoder(base64.StdEncoding, strings.NewReader(enc))).Decode(v)
}

// md5 hash calculator
func Md5HashCalc(data string) (string, error) {
	// decodedData, err := base64Decode(data)
	// if err != nil {
	// 	return "", err
	// }

	hmd5 := md5.Sum([]byte(data))
	// hsha1 := sha1.Sum([]byte(s))
	// hsha2 := sha256.Sum256([]byte(s))

	// fmt.Printf("   MD5: %x\n", hmd5)
	// fmt.Printf("  SHA1: %x\n", hsha1)
	// fmt.Printf("SHA256: %x\n", hsha2)

	return fmt.Sprintf("%x", hmd5), nil
}
