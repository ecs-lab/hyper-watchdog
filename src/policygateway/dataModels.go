package main

import "time"

// Data structures for unmarshaling JSON messages
type SensorData struct {
	X []float64 `json:"x"`
	Y []float64 `json:"y"`
	Z []float64 `json:"z"`
}
type Data struct {
	Id         string     `json:"id"`
	Sensor0    SensorData `json:"0"`
	Sensor1    SensorData `json:"1"`
	Sensor2    SensorData `json:"2"`
	Sensor3    SensorData `json:"3"`
	Sensor4    SensorData `json:"4"`
	Sensor5    SensorData `json:"5"`
	ReceivedOn time.Time
}

type QueryResponse struct {
	assets []QueryResponseAsset
}

type QueryResponseAsset struct {
	AppliedPolicyId string `json:"AppliedPolicyId"`
	ChunkId         string `json:"ChunkId"`
	DataHash        string `json:"DataHash"`
	ExpiryDate      string `json:"ExpiryDate"`
	ExpiryPeriod    int    `json:"ExpiryPeriod"`
}
