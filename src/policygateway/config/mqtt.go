package config

type MqttConfiguration struct {
	ClientId          string
	TransportProtocol string
	BrokerHostname    string
	BrokerPort        int
	Username          string
	Password          string
}
