package config

import (
	"log"

	"github.com/spf13/viper"
)

type Configuration struct {
	DataBlocks DataBlocksConfiguration
	Mqtt       MqttConfiguration
	Fabric     FabricConfiguration
}

var c Configuration

func init() {
	// Load configuration
	viper.SetConfigName("config")
	viper.SetConfigType("yml")
	viper.AddConfigPath(".")

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Error reading config file, %s", err)
	}
	err := viper.Unmarshal(&c)
	if err != nil {
		log.Fatalf("unable to decode into struct, %v", err)
	}

	log.Println("loaded config:\n", c)
}

func GetCurrentConfig() *Configuration {
	return &c
}
