/*
SPDX-License-Identifier: Apache-2.0
*/

package main

import (
	"log"

	"github.com/hyperledger/fabric-contract-api-go/contractapi"
	"github.com/hyperledger/fabric-samples/asset-transfer-basic/chaincode-go/chaincode"
)

func main() {
	policyChaincode, err := contractapi.NewChaincode(&chaincode.SmartContract{})
	if err != nil {
		log.Panicf("Error creating policy-smart-contract chaincode: %v", err)
	}

	if err := policyChaincode.Start(); err != nil {
		log.Panicf("Error starting policy-smart-contract chaincode: %v", err)
	}
}
