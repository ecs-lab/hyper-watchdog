package main

import (
	"fmt"
	"log"
	"policywatchdog/config"
	"sync"
	"time"
)

var c *config.Configuration = config.GetCurrentConfig()

func main() {
	contract := setupFabricSDK()

	var wg sync.WaitGroup
	wg.Add(1)
	go runLogic(contract, time.Second*time.Duration(c.Watchdog.ActionInterval))

	wg.Wait()
}

func timeTrackLog(start time.Time, name string) {
	elapsed := time.Since(start)
	log.Printf("timeTrackLog: %s took %s", name, elapsed)
}

func durationToMsString(d time.Duration) string {
	ms := d / time.Millisecond
	return fmt.Sprintf("%d", ms)
}
