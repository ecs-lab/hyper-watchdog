package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"

	fabricConfig "github.com/hyperledger/fabric-sdk-go/pkg/core/config"
	fabricGateway "github.com/hyperledger/fabric-sdk-go/pkg/gateway"
)

func setupFabricSDK() fabricGateway.Contract {
	// Delete old wallet file
	e := os.Remove("wallet/appUser.id")
	if e != nil {
		log.Println("setupFabricSDK: Old wallet file not found.")
	} else {
		log.Println("setupFabricSDK: Old wallet file found and deleted.")
	}

	err := os.Setenv("DISCOVERY_AS_LOCALHOST", "true")
	if err != nil {
		log.Fatalf("setupFabricSDK: Error setting DISCOVERY_AS_LOCALHOST environment variable: %v", err)
	}

	wallet, err := fabricGateway.NewFileSystemWallet("wallet")
	if err != nil {
		log.Fatalf("setupFabricSDK: Failed to create wallet: %v", err)
	}

	if !wallet.Exists("appUser") {
		err = populateWallet(wallet)
		if err != nil {
			log.Fatalf("setupFabricSDK: Failed to populate wallet contents: %v", err)
		}
	}

	ccpPath := c.Fabric.CcpPath
	// ccpPath := filepath.Join(
	// 	"..",
	// 	"..",
	// 	"..",
	// 	"test-network",
	// 	"organizations",
	// 	"peerOrganizations",
	// 	"org1.example.com",
	// 	"connection-org1.yaml",
	// )

	gw, err := fabricGateway.Connect(
		fabricGateway.WithConfig(fabricConfig.FromFile(filepath.Clean(ccpPath))),
		fabricGateway.WithIdentity(wallet, "appUser"),
	)
	if err != nil {
		log.Fatalf("setupFabricSDK: Failed to connect to Fabric gateway: %v", err)
	}
	defer gw.Close()

	network, err := gw.GetNetwork("mychannel")
	if err != nil {
		log.Fatalf("setupFabricSDK: Failed to get network: %v", err)
	}

	contract := network.GetContract("policy")
	return *contract
}

func populateWallet(wallet *fabricGateway.Wallet) error {
	log.Println("setupFabricSDK: Populating wallet...")
	credPath := c.Fabric.CredPath
	// credPath := filepath.Join(
	// 	"..",
	// 	"..",
	// 	"..",
	// 	"test-network",
	// 	"organizations",
	// 	"peerOrganizations",
	// 	"org1.example.com",
	// 	"users",
	// 	"User1@org1.example.com",
	// 	"msp",
	// )

	certPath := filepath.Join(credPath, "signcerts", "cert.pem")
	// read the certificate pem
	cert, err := ioutil.ReadFile(filepath.Clean(certPath))
	if err != nil {
		return err
	}

	keyDir := filepath.Join(credPath, "keystore")
	// there's a single file in this dir containing the private key
	files, err := ioutil.ReadDir(keyDir)
	if err != nil {
		return err
	}
	if len(files) != 1 {
		return fmt.Errorf("setupFabricSDK: keystore folder should have contain one file")
	}
	keyPath := filepath.Join(keyDir, files[0].Name())
	key, err := ioutil.ReadFile(filepath.Clean(keyPath))
	if err != nil {
		return err
	}

	identity := fabricGateway.NewX509Identity("Org1MSP", string(cert), string(key))

	return wallet.Put("appUser", identity)
}
