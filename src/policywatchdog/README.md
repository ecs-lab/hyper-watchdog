# README

## App settings

One may change settings by editing `config.yml`. This operation can be done only **before** building the Docker image.

## Testing locally

Set up the Go environment:

```sh
go mod init policygateway
go mod tidy
go get
```

Run the application:

```sh
go run policygateway
```
