package main

import (
	"encoding/json"
	"log"
	"time"

	fabricGateway "github.com/hyperledger/fabric-sdk-go/pkg/gateway"
)

func runLogic(contract fabricGateway.Contract, interval time.Duration) {
	for {
		select {
		case <-time.After(interval):
			// Policy watchdog: GetExpiredChunks
			func() {
				expiredQueryResponse := func() []byte {
					defer timeTrackLog(time.Now(), "GetExpiredChunks")

					log.Println("--> Evaluate Transaction: GetExpiredChunks, function returns expired chunks")
					result, err := contract.SubmitTransaction("GetExpiredChunks", time.Now().Format(time.RFC3339))
					if err != nil {
						log.Printf("Failed to evaluate transaction: %v\n", err)
					}
					if len(result) == 0 {
						log.Println("Empty list")
					}
					log.Println(string(result))

					return result
				}()

				expiredData := []QueryResponseAsset{} // For Json unmarshaling
				json.Unmarshal(expiredQueryResponse, &expiredData)

				for _, chunk := range expiredData {
					func() {
						defer timeTrackLog(time.Now(), "DeleteChunkIfExpired")
						log.Println("--> Evaluate Transaction: DeleteChunkIfExpired, function deletes expired chunk")
						result, err := contract.SubmitTransaction("DeleteChunkIfExpired", chunk.ChunkId)
						if err != nil {
							log.Printf("Failed to evaluate transaction: %v\n", err)
						}
						log.Println(string(result))
					}()
				}
			}()
		}
	}
}
