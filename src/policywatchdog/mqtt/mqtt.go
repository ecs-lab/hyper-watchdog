package mqtt

import (
	"fmt"
	"log"
	"time"

	"policywatchdog/config"

	pahoMqtt "github.com/eclipse/paho.mqtt.golang"
)

var c *config.Configuration

var client pahoMqtt.Client

var subChan chan string

var connectHandler pahoMqtt.OnConnectHandler = func(client pahoMqtt.Client) {
	log.Println("pahoMqtt.OnConnectHandler: Connected")
	st := subscribedTopics.get()
	for _, t := range st {
		subscribe(client, t, mySubHandler)
	}
}

var connectLostHandler pahoMqtt.ConnectionLostHandler = func(client pahoMqtt.Client, err error) {
	log.Printf("pahoMqtt.ConnectionLostHandler: Connection lost: %v", err)
}

type SubscribedTopics struct {
	list []string
}

func (s *SubscribedTopics) contains(t string) bool {
	for _, st := range s.list {
		if st == t {
			return true
		}
	}
	return false
}

func (s *SubscribedTopics) add(t string) {
	if !s.contains(t) {
		s.list = append(s.list, t)
	}
}

func (s *SubscribedTopics) get() []string {
	return s.list
}

var subscribedTopics SubscribedTopics = SubscribedTopics{[]string{}}

func subscribe(client pahoMqtt.Client, topic string, handler pahoMqtt.MessageHandler) {
	if token := client.Subscribe(topic, 0, handler); token.Wait() && token.Error() != nil {
		fmt.Println(token.Error())
	}
	subscribedTopics.add(topic)
}

var mySubHandler pahoMqtt.MessageHandler = func(client pahoMqtt.Client, message pahoMqtt.Message) {
	log.Printf("pahoMqtt.MessageHandler: Message received on topic: %q\n", message.Topic())
	// fmt.Printf("Payload: %s\n", message.Payload())

	if message.Topic() == "data" {
		subChan <- fmt.Sprintf("%s", message.Payload())
	}
}

func init() {
	// Load current configuration
	c = config.GetCurrentConfig()

	broker := fmt.Sprintf("%s://%s:%d", c.Mqtt.TransportProtocol, c.Mqtt.BrokerHostname, c.Mqtt.BrokerPort)
	mqttClientOpt := pahoMqtt.NewClientOptions().AddBroker(broker).
		SetAutoReconnect(true).
		SetCleanSession(true).
		SetPingTimeout(1 * time.Hour).
		SetClientID(c.Mqtt.ClientId)

	if c.Mqtt.Username != "" {
		log.Println("Using ", c.Mqtt.Username, " as username")
		mqttClientOpt = mqttClientOpt.SetUsername(c.Mqtt.Username)
	}
	if c.Mqtt.Password != "" {
		log.Println("Using ", c.Mqtt.Password, " as password")
		mqttClientOpt = mqttClientOpt.SetUsername(c.Mqtt.Password)
	}

	mqttClientOpt.OnConnect = connectHandler
	mqttClientOpt.OnConnectionLost = connectLostHandler

	client = pahoMqtt.NewClient(mqttClientOpt)
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}
}

func SubscribeAndListen() <-chan string {
	subChan = make(chan string)
	subscribe(client, c.DataBlocks.MqttTopic, mySubHandler)
	return subChan
}

func Disconnect() {
	client.Disconnect(0)
}
